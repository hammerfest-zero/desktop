# [WIP] Desktop

This is an electron-powered desktop version of the game. It's basically a wrapper around the Hammerfest Zero website, with a pre-installed version of Flash Player.  
This allows to play the game without having to install an up-to-date flash player (which can be a pain these days). This will also allow you to play the game after the end of 2020.

Please note that this is still Work in progress, and that some feature are still missing.
