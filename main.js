#!/usr/bin/env node

const fs = require('fs').promises
const { app, BrowserWindow } = require('electron')
const windowStateKeeper = require('electron-window-state')
const path = require('path')

const pluginNames = {
  'win32': 'pepflashplayer.dll',
  'darwin': 'PepperFlashPlayer.plugin',
  'linux': 'libpepflashplayer.so'
}
app.commandLine.appendSwitch('ppapi-flash-path', path.join(__dirname, 'flash', pluginNames[process.platform]))

const DOMAIN = "https://zerofe.st"

async function writeMms() {
  const dirPath  = path.join(app.getPath('userData'), 'Pepper Data', 'Shockwave Flash', 'System')
  const filePath = path.join(dirPath, 'mms.cfg')
  await fs.mkdir(dirPath, {recursive: true})
  await fs.writeFile(filePath,
                     '# Do no edit this file by hand, your changes will be overwritten\n\n'
                     + 'EnableAllowList=1\n'
                     + 'AllowListUrlPattern=' + DOMAIN + '\n')
}

writeMms()

app.whenReady().then(() => {
  const mainWindowState = windowStateKeeper({
    defaultWidth: 800,
    defaultHeight: 600
  })

  const win = new BrowserWindow({
    x: mainWindowState.x,
    y: mainWindowState.y,
    width: mainWindowState.width,
    height: mainWindowState.height,
    webPreferences: {
      plugins: true
    }
  })

  mainWindowState.manage(win)
  win.loadURL(DOMAIN)
})

app.on('window-all-closed', () => {
  app.quit()
})
